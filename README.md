# `load-portfolio-performance-xml`

## Contents

An **unofficial** Javascript/Typescript loader for the XML files of the
[Portfolio Performance](https://www.portfolio-performance.info/) application.

Developed and tested with version `56` of the application's file format.

Can be used on the client side (browser environments) or on the server side (node.js).

## Usage

At this time, the library implements only a generic data model of nodes, but no specific type definitions for the PP data model.

**For a specification of the available fields, please refer to the data
[model](https://github.com/buchen/portfolio/tree/master/name.abuchen.portfolio/src/name/abuchen/portfolio/model)
directory in the Portfolio Performance repository.**

```ts
import { readFileSync } from "fs"
import { loadPortfolioPerformanceXml } from "."

try {
  const xml = readFileSync("data/portfolio-performance.xml")
  const data = loadPortfolioPerformanceXml(xml) // accepts `string | Buffer`

  console.log("File Version: " + data.client?.[0]?.version?.[0]?.["#text"] || "n/a")
  // File Version: 56

  console.log("Accounts: " + data.client?.[0]?.accounts?.[0]?.account?.length || 0)
  // Accounts: 3

  console.log("Securities: " + data.client?.[0]?.securities?.[0]?.security?.length || 0)
  // Securities: 12

  console.log("Example Security Name: " + data.client?.[0]?.securities?.[0]?.security?.[0].name?.[0]["#text"] || "n/a")
  // Example Security Name: Deka DAX
} catch (err) {
  console.error("oh oh", err)
}
```

## Implementation Details

Portfolio Performance is a Java application. Its data model uses native object references in a directed graph. Such data models typically
require deduplication during serialization and reference resolution during deserialization to avoid data corruption.

Portfolio Performance [uses](https://forum.portfolio-performance.info/t/referenzen-in-der-struktur-der-xml-datei/3514) the
[XStream](https://x-stream.github.io/) library for this purpose. XStream implements an algorithm that only renders the first occurance of
an object as a fully populated XML node; all subsequent occurences are rendered as empty XML tags with a single `reference` attribute that
holds a stringified, relative path to the fully populated node, such as
`"../../../../../../../../../../../../../../../../../../../../../../../../../../../securities/security"`.
If more nodes with the same tag name should be encountered during serialization, the original node is converted to an array of nodes,
and the original node at this position becomes the first entry of the array. References to array entries _may_ contain an index; if the
target of a path segment is an array and the path segment contains no index, it is resolved as pointing to the first array entry.
If present, index values in XStream generally start at `1` and not at `0` as in Javascript.

This library delegates the XML processing in the first step of its parser process to the battle-proven
[`fast-xml-parser`](https://www.npmjs.com/package/fast-xml-parser) library, with a configuration that seems to work well for
Portfolio Performance files. Afterwards, we resolve all references in an XStream-compliant manner. This second step is hand-crafted,
as there seems to be no Javascript port of the XStream library or its reference resolution algorithm at this time.

The returned data structure is intentionally rather generic (consisting of nested `Node` and `NodeArray` instances) because the
availability of nodes can vary in different PP file format versions.

## Copyright / License

Copyright (c) 2022 Daniel Kastenholz.

All rights reserved.

Licensed under the terms of the [AGPL-3.0](https://spdx.org/licenses/AGPL-3.0-only.html) **only**.

## History

| Version | Changes                           |
| ------- | --------------------------------- |
| 1.0.0   | Initial version                   |
| 1.0.1   | Bugfix: deep-clone in `resolve()` |
| 1.0.2   | Bugfix: reference resolution      |
| 1.0.3   | README update                     |
