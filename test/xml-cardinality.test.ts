import { resolve, ResolvedNode } from "../src/resolve"
import { transform, TransformedNode } from "../src/transform"
import { parseXml, XmlNode } from "../src/xml"

const mock = `
<root>
  <singleton />
  <non-singleton />
  <non-singleton />
  <non-singleton />
</root>
`

describe("xml cardinality", () => {
  let xml: XmlNode, tn: TransformedNode, rn: ResolvedNode

  test("xml", () => {
    xml = parseXml(mock)
    expect(xml).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "non-singleton": Array [
              "",
              "",
              "",
            ],
            "singleton": Array [
              "",
            ],
          },
        ],
      }
    `)
  })

  test("transform", () => {
    tn = transform(xml)
    expect(parseXml(mock)).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "non-singleton": Array [
              "",
              "",
              "",
            ],
            "singleton": Array [
              "",
            ],
          },
        ],
      }
    `)
  })

  test("resolve", () => {
    rn = resolve(tn)
    expect(rn).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "non-singleton": Array [
              Object {
                "#text": "",
              },
              Object {
                "#text": "",
              },
              Object {
                "#text": "",
              },
            ],
            "singleton": Array [
              Object {
                "#text": "",
              },
            ],
          },
        ],
      }
    `)
  })
})
