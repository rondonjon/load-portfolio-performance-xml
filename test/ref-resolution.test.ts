import nodeFetch from "node-fetch"
import { transform, TransformedNode } from "../src/transform"
import { parseXml } from "../src/xml"
import { resolve, ResolvedNode } from "../src/resolve"
import * as parseReference from "../src/parseReference"

const countReferences = (node: TransformedNode | ResolvedNode, seen: Set<TransformedNode | ResolvedNode>) => {
  seen.add(node)

  if (node["#reference"]) {
    return 1
  } else {
    let count = 0

    for (const key in node) {
      if (!key.startsWith("#")) {
        for (const childNode of node[key]) {
          if (!seen.has(childNode)) {
            count += countReferences(childNode, seen)
          }
        }
      }
    }

    return count
  }
}

describe("ref resolution", () => {
  let mock: string, tn: TransformedNode, rn: ResolvedNode, refCount: number

  beforeAll(async () => {
    const response = await nodeFetch(
      "https://raw.githubusercontent.com/buchen/portfolio/master/name.abuchen.portfolio.ui/src/name/abuchen/portfolio/ui/parts/kommer.xml"
    )

    mock = await response.text()
  })

  test("downloaded valid xml mock file", () => {
    const xml = parseXml(mock)
    tn = transform(xml)
    expect(tn).not.toBeFalsy()
  })

  test("found min. 50 distinct nodes and min. 10 references", () => {
    const seen = new Set<TransformedNode | ResolvedNode>()
    refCount = countReferences(tn, seen)
    expect(seen.size).toBeGreaterThan(50)
    expect(refCount).toBeGreaterThan(10)
  })

  test("called parseReference() for every reference", () => {
    const spy = jest.spyOn(parseReference, "parseReference")
    rn = resolve(tn)
    const parseReferenceCount = spy.mock.calls.length
    expect(parseReferenceCount).toBe(refCount)
  })

  test("resolved a reference-free graph", () => {
    const resolveRefCount = countReferences(rn, new Set())

    expect(resolveRefCount).toBe(0)
  })
})
