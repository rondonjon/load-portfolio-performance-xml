import { resolve, ResolvedNode } from "../src/resolve"
import { transform, TransformedNode } from "../src/transform"
import { parseXml, XmlNode } from "../src/xml"

const mock = `
<root>
  <data string="a" number="2" boolean />
</root>
`

describe("xml attributes", () => {
  let xml: XmlNode, tn: TransformedNode, rn: ResolvedNode

  test("xml", () => {
    xml = parseXml(mock)
    expect(xml).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "data": Array [
              Object {
                "number": Array [
                  "2",
                ],
                "string": Array [
                  "a",
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("transform", () => {
    tn = transform(xml)
    expect(parseXml(mock)).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "data": Array [
              Object {
                "number": Array [
                  "2",
                ],
                "string": Array [
                  "a",
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("resolve", () => {
    rn = resolve(tn)
    expect(rn).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "data": Array [
              Object {
                "number": Array [
                  Object {
                    "#text": "2",
                  },
                ],
                "string": Array [
                  Object {
                    "#text": "a",
                  },
                ],
              },
            ],
          },
        ],
      }
    `)
  })
})
