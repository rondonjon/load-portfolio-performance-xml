import { resolve, ResolvedNode } from "../src/resolve"
import { transform, TransformedNode } from "../src/transform"
import { parseXml, XmlNode } from "../src/xml"

const mock = `
<root>
  <text>
    content<foo />content
  </text>
</root>
`

describe("xml text content", () => {
  let xml: XmlNode, tn: TransformedNode, rn: ResolvedNode

  test("xml", () => {
    xml = parseXml(mock)
    expect(xml).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "text": Array [
              Object {
                "#text": "contentcontent",
                "foo": Array [
                  "",
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("transform", () => {
    tn = transform(xml)
    expect(parseXml(mock)).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "text": Array [
              Object {
                "#text": "contentcontent",
                "foo": Array [
                  "",
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("resolve", () => {
    rn = resolve(tn)
    expect(rn).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "text": Array [
              Object {
                "#text": "contentcontent",
                "foo": Array [
                  Object {
                    "#text": "",
                  },
                ],
              },
            ],
          },
        ],
      }
    `)
  })
})
