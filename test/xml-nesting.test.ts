import { resolve, ResolvedNode } from "../src/resolve"
import { transform, TransformedNode } from "../src/transform"
import { parseXml, XmlNode } from "../src/xml"

const mock = `
<root>
  <level1>
    <level2 />
  </level1>
</root>
`

describe("xml nesting", () => {
  let xml: XmlNode, tn: TransformedNode, rn: ResolvedNode

  test("xml", () => {
    xml = parseXml(mock)
    expect(xml).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "level1": Array [
              Object {
                "level2": Array [
                  "",
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("transform", () => {
    tn = transform(xml)
    expect(parseXml(mock)).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "level1": Array [
              Object {
                "level2": Array [
                  "",
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("resolve", () => {
    rn = resolve(tn)
    expect(rn).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "level1": Array [
              Object {
                "level2": Array [
                  Object {
                    "#text": "",
                  },
                ],
              },
            ],
          },
        ],
      }
    `)
  })
})
