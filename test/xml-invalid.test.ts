import { parseXml } from "../src/xml"

describe("raises error for invalid xml", () => {
  test("empty string", () => {
    expect(() => {
      console.log(parseXml(""))
    }).toThrow()
  })

  test("plain text", () => {
    expect(() => {
      console.log(parseXml("foo bar"))
    }).toThrow()
  })
})
