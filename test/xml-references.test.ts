import { resolve, ResolvedNode } from "../src/resolve"
import { transform, TransformedNode } from "../src/transform"
import { parseXml, XmlNode } from "../src/xml"

const mock = `
<root>
  <a id="1" />
  <a id="2" />
  <very>
    <deep>
      <nesting>
        <a id="3" />
      </nesting>
    </deep>
  </very>
  <references>
    <ref reference="../../a" />
    <ref reference="../../a[1]" />
    <ref reference="../../a[2]" />
    <ref reference="../../very/deep/nesting/a" />
    <ref reference="../../very/deep/nesting/a[1]" />
  </references>
</root>
`

describe("xml references", () => {
  let xml: XmlNode, tn: TransformedNode, rn: ResolvedNode

  test("xml", () => {
    xml = parseXml(mock)
    expect(xml).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "a": Array [
              Object {
                "id": Array [
                  "1",
                ],
              },
              Object {
                "id": Array [
                  "2",
                ],
              },
            ],
            "references": Array [
              Object {
                "ref": Array [
                  Object {
                    "reference": Array [
                      "../../a",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../a[1]",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../a[2]",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../very/deep/nesting/a",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../very/deep/nesting/a[1]",
                    ],
                  },
                ],
              },
            ],
            "very": Array [
              Object {
                "deep": Array [
                  Object {
                    "nesting": Array [
                      Object {
                        "a": Array [
                          Object {
                            "id": Array [
                              "3",
                            ],
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("transform", () => {
    tn = transform(xml)
    expect(parseXml(mock)).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "a": Array [
              Object {
                "id": Array [
                  "1",
                ],
              },
              Object {
                "id": Array [
                  "2",
                ],
              },
            ],
            "references": Array [
              Object {
                "ref": Array [
                  Object {
                    "reference": Array [
                      "../../a",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../a[1]",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../a[2]",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../very/deep/nesting/a",
                    ],
                  },
                  Object {
                    "reference": Array [
                      "../../very/deep/nesting/a[1]",
                    ],
                  },
                ],
              },
            ],
            "very": Array [
              Object {
                "deep": Array [
                  Object {
                    "nesting": Array [
                      Object {
                        "a": Array [
                          Object {
                            "id": Array [
                              "3",
                            ],
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      }
    `)
  })

  test("resolve", () => {
    rn = resolve(tn)
    expect(rn).toMatchInlineSnapshot(`
      Object {
        "root": Array [
          Object {
            "a": Array [
              Object {
                "id": Array [
                  Object {
                    "#text": "1",
                  },
                ],
              },
              Object {
                "id": Array [
                  Object {
                    "#text": "2",
                  },
                ],
              },
            ],
            "references": Array [
              Object {
                "ref": Array [
                  Object {
                    "id": Array [
                      Object {
                        "#text": "1",
                      },
                    ],
                  },
                  Object {
                    "id": Array [
                      Object {
                        "#text": "1",
                      },
                    ],
                  },
                  Object {
                    "id": Array [
                      Object {
                        "#text": "2",
                      },
                    ],
                  },
                  Object {
                    "id": Array [
                      Object {
                        "#text": "3",
                      },
                    ],
                  },
                  Object {
                    "id": Array [
                      Object {
                        "#text": "3",
                      },
                    ],
                  },
                ],
              },
            ],
            "very": Array [
              Object {
                "deep": Array [
                  Object {
                    "nesting": Array [
                      Object {
                        "a": Array [
                          Object {
                            "id": Array [
                              Object {
                                "#text": "3",
                              },
                            ],
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      }
    `)
  })
})
