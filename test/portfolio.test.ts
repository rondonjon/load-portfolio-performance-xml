import { readFileSync } from "fs"
import { join } from "path"
import { resolve, ResolvedNode } from "../src/resolve"
import { transform, TransformedNode } from "../src/transform"
import { parseXml, XmlNode } from "../src/xml"

const mock = readFileSync(join(__dirname, "portfolio.xml")) // generated with PP

describe("portfolio", () => {
  let xml: XmlNode, tn: TransformedNode, rn: ResolvedNode

  test("xml", () => {
    xml = parseXml(mock)
    expect(xml).toMatchInlineSnapshot(`
      Object {
        "client": Array [
          Object {
            "accounts": Array [
              Object {
                "account": Array [
                  Object {
                    "currencyCode": Array [
                      "EUR",
                    ],
                    "isRetired": Array [
                      "false",
                    ],
                    "name": Array [
                      "Referenzkonto",
                    ],
                    "transactions": Array [
                      "",
                    ],
                    "updatedAt": Array [
                      "2022-08-16T15:07:14.719690500Z",
                    ],
                    "uuid": Array [
                      "e030221d-bd61-4d2d-8abb-88220122aaf1",
                    ],
                  },
                ],
              },
            ],
            "baseCurrency": Array [
              "EUR",
            ],
            "dashboards": Array [
              "",
            ],
            "plans": Array [
              "",
            ],
            "portfolios": Array [
              Object {
                "portfolio": Array [
                  Object {
                    "isRetired": Array [
                      "false",
                    ],
                    "name": Array [
                      "Depot",
                    ],
                    "referenceAccount": Array [
                      Object {
                        "reference": Array [
                          "../../../accounts/account",
                        ],
                      },
                    ],
                    "transactions": Array [
                      "",
                    ],
                    "updatedAt": Array [
                      "2022-08-16T15:06:51.951256600Z",
                    ],
                    "uuid": Array [
                      "a51f72a1-e514-4428-aab3-5f7c5547e169",
                    ],
                  },
                ],
              },
            ],
            "properties": Array [
              "",
            ],
            "securities": Array [
              "",
            ],
            "settings": Array [
              Object {
                "attributeTypes": Array [
                  Object {
                    "attribute-type": Array [
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Account",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Portfolio",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.InvestmentPlan",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "TER",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                        ],
                        "id": Array [
                          "ter",
                        ],
                        "name": Array [
                          "Gesamtkostenquote (TER)",
                        ],
                        "source": Array [
                          "ter",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Double",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Fondsgröße",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$AmountPlainConverter",
                        ],
                        "id": Array [
                          "aum",
                        ],
                        "name": Array [
                          "Fondsgröße",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Long",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Anbieter",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$StringConverter",
                        ],
                        "id": Array [
                          "vendor",
                        ],
                        "name": Array [
                          "Anbieter",
                        ],
                        "source": Array [
                          "vendor",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Kaufgebühr",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                        ],
                        "id": Array [
                          "acquisitionFee",
                        ],
                        "name": Array [
                          "Kaufgebühr (prozentual)",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Double",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Verwaltungsgebühr",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                        ],
                        "id": Array [
                          "managementFee",
                        ],
                        "name": Array [
                          "Verwaltungsgebühr (prozentual)",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Double",
                        ],
                      },
                    ],
                  },
                ],
                "bookmarks": Array [
                  Object {
                    "bookmark": Array [
                      Object {
                        "label": Array [
                          "Yahoo Finance",
                        ],
                        "pattern": Array [
                          "http://de.finance.yahoo.com/q?s={tickerSymbol}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "OnVista",
                        ],
                        "pattern": Array [
                          "http://www.onvista.de/suche.html?SEARCH_VALUE={isin}&SELECTED_TOOL=ALL_TOOLS",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Finanzen.net",
                        ],
                        "pattern": Array [
                          "http://www.finanzen.net/suchergebnis.asp?frmAktiensucheTextfeld={isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Ariva.de Fundamentaldaten",
                        ],
                        "pattern": Array [
                          "http://www.ariva.de/{isin}/bilanz-guv",
                        ],
                      },
                      Object {
                        "label": Array [
                          "justETF",
                        ],
                        "pattern": Array [
                          "https://www.justetf.com/de/etf-profile.html?isin={isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "fondsweb.de",
                        ],
                        "pattern": Array [
                          "http://www.fondsweb.de/{isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Morningstar.de",
                        ],
                        "pattern": Array [
                          "http://www.morningstar.de/de/funds/SecuritySearchResults.aspx?type=ALL&search={isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "extraETF.com",
                        ],
                        "pattern": Array [
                          "https://extraetf.com/etf-profile/{isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Alle Aktien Kennzahlen",
                        ],
                        "pattern": Array [
                          "https://www.alleaktien.de/quantitativ/{isin}/",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Comdirect (Aktien)",
                        ],
                        "pattern": Array [
                          "https://www.comdirect.de/inf/aktien/{isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "DivvyDiary",
                        ],
                        "pattern": Array [
                          "https://divvydiary.com/symbols/{isin}",
                        ],
                      },
                    ],
                  },
                ],
                "configurationSets": Array [
                  Object {
                    "entry": Array [
                      Object {
                        "config-set": Array [
                          Object {
                            "configurations": Array [
                              Object {
                                "config": Array [
                                  Object {
                                    "name": Array [
                                      "Standard",
                                    ],
                                    "uuid": Array [
                                      "afca6aa8-df77-40af-9cfb-6af99a661b11",
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                        "string": Array [
                          "name.abuchen.portfolio.ui.views.StatementOfAssetsViewer",
                        ],
                      },
                    ],
                  },
                ],
              },
            ],
            "taxonomies": Array [
              "",
            ],
            "version": Array [
              "56",
            ],
            "watchlists": Array [
              "",
            ],
          },
        ],
      }
    `)
  })

  test("transform", () => {
    tn = transform(xml)
    expect(parseXml(mock)).toMatchInlineSnapshot(`
      Object {
        "client": Array [
          Object {
            "accounts": Array [
              Object {
                "account": Array [
                  Object {
                    "currencyCode": Array [
                      "EUR",
                    ],
                    "isRetired": Array [
                      "false",
                    ],
                    "name": Array [
                      "Referenzkonto",
                    ],
                    "transactions": Array [
                      "",
                    ],
                    "updatedAt": Array [
                      "2022-08-16T15:07:14.719690500Z",
                    ],
                    "uuid": Array [
                      "e030221d-bd61-4d2d-8abb-88220122aaf1",
                    ],
                  },
                ],
              },
            ],
            "baseCurrency": Array [
              "EUR",
            ],
            "dashboards": Array [
              "",
            ],
            "plans": Array [
              "",
            ],
            "portfolios": Array [
              Object {
                "portfolio": Array [
                  Object {
                    "isRetired": Array [
                      "false",
                    ],
                    "name": Array [
                      "Depot",
                    ],
                    "referenceAccount": Array [
                      Object {
                        "reference": Array [
                          "../../../accounts/account",
                        ],
                      },
                    ],
                    "transactions": Array [
                      "",
                    ],
                    "updatedAt": Array [
                      "2022-08-16T15:06:51.951256600Z",
                    ],
                    "uuid": Array [
                      "a51f72a1-e514-4428-aab3-5f7c5547e169",
                    ],
                  },
                ],
              },
            ],
            "properties": Array [
              "",
            ],
            "securities": Array [
              "",
            ],
            "settings": Array [
              Object {
                "attributeTypes": Array [
                  Object {
                    "attribute-type": Array [
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Account",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Portfolio",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Logo",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                        ],
                        "id": Array [
                          "logo",
                        ],
                        "name": Array [
                          "Logo",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.InvestmentPlan",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "TER",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                        ],
                        "id": Array [
                          "ter",
                        ],
                        "name": Array [
                          "Gesamtkostenquote (TER)",
                        ],
                        "source": Array [
                          "ter",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Double",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Fondsgröße",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$AmountPlainConverter",
                        ],
                        "id": Array [
                          "aum",
                        ],
                        "name": Array [
                          "Fondsgröße",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Long",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Anbieter",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$StringConverter",
                        ],
                        "id": Array [
                          "vendor",
                        ],
                        "name": Array [
                          "Anbieter",
                        ],
                        "source": Array [
                          "vendor",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.String",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Kaufgebühr",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                        ],
                        "id": Array [
                          "acquisitionFee",
                        ],
                        "name": Array [
                          "Kaufgebühr (prozentual)",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Double",
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          "Verwaltungsgebühr",
                        ],
                        "converterClass": Array [
                          "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                        ],
                        "id": Array [
                          "managementFee",
                        ],
                        "name": Array [
                          "Verwaltungsgebühr (prozentual)",
                        ],
                        "target": Array [
                          "name.abuchen.portfolio.model.Security",
                        ],
                        "type": Array [
                          "java.lang.Double",
                        ],
                      },
                    ],
                  },
                ],
                "bookmarks": Array [
                  Object {
                    "bookmark": Array [
                      Object {
                        "label": Array [
                          "Yahoo Finance",
                        ],
                        "pattern": Array [
                          "http://de.finance.yahoo.com/q?s={tickerSymbol}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "OnVista",
                        ],
                        "pattern": Array [
                          "http://www.onvista.de/suche.html?SEARCH_VALUE={isin}&SELECTED_TOOL=ALL_TOOLS",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Finanzen.net",
                        ],
                        "pattern": Array [
                          "http://www.finanzen.net/suchergebnis.asp?frmAktiensucheTextfeld={isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Ariva.de Fundamentaldaten",
                        ],
                        "pattern": Array [
                          "http://www.ariva.de/{isin}/bilanz-guv",
                        ],
                      },
                      Object {
                        "label": Array [
                          "justETF",
                        ],
                        "pattern": Array [
                          "https://www.justetf.com/de/etf-profile.html?isin={isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "fondsweb.de",
                        ],
                        "pattern": Array [
                          "http://www.fondsweb.de/{isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Morningstar.de",
                        ],
                        "pattern": Array [
                          "http://www.morningstar.de/de/funds/SecuritySearchResults.aspx?type=ALL&search={isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "extraETF.com",
                        ],
                        "pattern": Array [
                          "https://extraetf.com/etf-profile/{isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Alle Aktien Kennzahlen",
                        ],
                        "pattern": Array [
                          "https://www.alleaktien.de/quantitativ/{isin}/",
                        ],
                      },
                      Object {
                        "label": Array [
                          "Comdirect (Aktien)",
                        ],
                        "pattern": Array [
                          "https://www.comdirect.de/inf/aktien/{isin}",
                        ],
                      },
                      Object {
                        "label": Array [
                          "DivvyDiary",
                        ],
                        "pattern": Array [
                          "https://divvydiary.com/symbols/{isin}",
                        ],
                      },
                    ],
                  },
                ],
                "configurationSets": Array [
                  Object {
                    "entry": Array [
                      Object {
                        "config-set": Array [
                          Object {
                            "configurations": Array [
                              Object {
                                "config": Array [
                                  Object {
                                    "name": Array [
                                      "Standard",
                                    ],
                                    "uuid": Array [
                                      "afca6aa8-df77-40af-9cfb-6af99a661b11",
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                        "string": Array [
                          "name.abuchen.portfolio.ui.views.StatementOfAssetsViewer",
                        ],
                      },
                    ],
                  },
                ],
              },
            ],
            "taxonomies": Array [
              "",
            ],
            "version": Array [
              "56",
            ],
            "watchlists": Array [
              "",
            ],
          },
        ],
      }
    `)
  })

  test("resolve", () => {
    rn = resolve(tn)
    expect(rn).toMatchInlineSnapshot(`
      Object {
        "client": Array [
          Object {
            "accounts": Array [
              Object {
                "account": Array [
                  Object {
                    "currencyCode": Array [
                      Object {
                        "#text": "EUR",
                      },
                    ],
                    "isRetired": Array [
                      Object {
                        "#text": "false",
                      },
                    ],
                    "name": Array [
                      Object {
                        "#text": "Referenzkonto",
                      },
                    ],
                    "transactions": Array [
                      Object {
                        "#text": "",
                      },
                    ],
                    "updatedAt": Array [
                      Object {
                        "#text": "2022-08-16T15:07:14.719690500Z",
                      },
                    ],
                    "uuid": Array [
                      Object {
                        "#text": "e030221d-bd61-4d2d-8abb-88220122aaf1",
                      },
                    ],
                  },
                ],
              },
            ],
            "baseCurrency": Array [
              Object {
                "#text": "EUR",
              },
            ],
            "dashboards": Array [
              Object {
                "#text": "",
              },
            ],
            "plans": Array [
              Object {
                "#text": "",
              },
            ],
            "portfolios": Array [
              Object {
                "portfolio": Array [
                  Object {
                    "isRetired": Array [
                      Object {
                        "#text": "false",
                      },
                    ],
                    "name": Array [
                      Object {
                        "#text": "Depot",
                      },
                    ],
                    "referenceAccount": Array [
                      Object {
                        "currencyCode": Array [
                          Object {
                            "#text": "EUR",
                          },
                        ],
                        "isRetired": Array [
                          Object {
                            "#text": "false",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Referenzkonto",
                          },
                        ],
                        "transactions": Array [
                          Object {
                            "#text": "",
                          },
                        ],
                        "updatedAt": Array [
                          Object {
                            "#text": "2022-08-16T15:07:14.719690500Z",
                          },
                        ],
                        "uuid": Array [
                          Object {
                            "#text": "e030221d-bd61-4d2d-8abb-88220122aaf1",
                          },
                        ],
                      },
                    ],
                    "transactions": Array [
                      Object {
                        "#text": "",
                      },
                    ],
                    "updatedAt": Array [
                      Object {
                        "#text": "2022-08-16T15:06:51.951256600Z",
                      },
                    ],
                    "uuid": Array [
                      Object {
                        "#text": "a51f72a1-e514-4428-aab3-5f7c5547e169",
                      },
                    ],
                  },
                ],
              },
            ],
            "properties": Array [
              Object {
                "#text": "",
              },
            ],
            "securities": Array [
              Object {
                "#text": "",
              },
            ],
            "settings": Array [
              Object {
                "attributeTypes": Array [
                  Object {
                    "attribute-type": Array [
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "logo",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Security",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.String",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "logo",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Account",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.String",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "logo",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Portfolio",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.String",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$ImageConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "logo",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Logo",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.InvestmentPlan",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.String",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "TER",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "ter",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Gesamtkostenquote (TER)",
                          },
                        ],
                        "source": Array [
                          Object {
                            "#text": "ter",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Security",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.Double",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Fondsgröße",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$AmountPlainConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "aum",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Fondsgröße",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Security",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.Long",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Anbieter",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$StringConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "vendor",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Anbieter",
                          },
                        ],
                        "source": Array [
                          Object {
                            "#text": "vendor",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Security",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.String",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Kaufgebühr",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "acquisitionFee",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Kaufgebühr (prozentual)",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Security",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.Double",
                          },
                        ],
                      },
                      Object {
                        "columnLabel": Array [
                          Object {
                            "#text": "Verwaltungsgebühr",
                          },
                        ],
                        "converterClass": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.AttributeType$PercentConverter",
                          },
                        ],
                        "id": Array [
                          Object {
                            "#text": "managementFee",
                          },
                        ],
                        "name": Array [
                          Object {
                            "#text": "Verwaltungsgebühr (prozentual)",
                          },
                        ],
                        "target": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.model.Security",
                          },
                        ],
                        "type": Array [
                          Object {
                            "#text": "java.lang.Double",
                          },
                        ],
                      },
                    ],
                  },
                ],
                "bookmarks": Array [
                  Object {
                    "bookmark": Array [
                      Object {
                        "label": Array [
                          Object {
                            "#text": "Yahoo Finance",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "http://de.finance.yahoo.com/q?s={tickerSymbol}",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "OnVista",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "http://www.onvista.de/suche.html?SEARCH_VALUE={isin}&SELECTED_TOOL=ALL_TOOLS",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "Finanzen.net",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "http://www.finanzen.net/suchergebnis.asp?frmAktiensucheTextfeld={isin}",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "Ariva.de Fundamentaldaten",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "http://www.ariva.de/{isin}/bilanz-guv",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "justETF",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "https://www.justetf.com/de/etf-profile.html?isin={isin}",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "fondsweb.de",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "http://www.fondsweb.de/{isin}",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "Morningstar.de",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "http://www.morningstar.de/de/funds/SecuritySearchResults.aspx?type=ALL&search={isin}",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "extraETF.com",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "https://extraetf.com/etf-profile/{isin}",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "Alle Aktien Kennzahlen",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "https://www.alleaktien.de/quantitativ/{isin}/",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "Comdirect (Aktien)",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "https://www.comdirect.de/inf/aktien/{isin}",
                          },
                        ],
                      },
                      Object {
                        "label": Array [
                          Object {
                            "#text": "DivvyDiary",
                          },
                        ],
                        "pattern": Array [
                          Object {
                            "#text": "https://divvydiary.com/symbols/{isin}",
                          },
                        ],
                      },
                    ],
                  },
                ],
                "configurationSets": Array [
                  Object {
                    "entry": Array [
                      Object {
                        "config-set": Array [
                          Object {
                            "configurations": Array [
                              Object {
                                "config": Array [
                                  Object {
                                    "name": Array [
                                      Object {
                                        "#text": "Standard",
                                      },
                                    ],
                                    "uuid": Array [
                                      Object {
                                        "#text": "afca6aa8-df77-40af-9cfb-6af99a661b11",
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                        "string": Array [
                          Object {
                            "#text": "name.abuchen.portfolio.ui.views.StatementOfAssetsViewer",
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
            ],
            "taxonomies": Array [
              Object {
                "#text": "",
              },
            ],
            "version": Array [
              Object {
                "#text": "56",
              },
            ],
            "watchlists": Array [
              Object {
                "#text": "",
              },
            ],
          },
        ],
      }
    `)
  })
})
