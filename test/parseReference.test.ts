import { parseReference } from "../src/parseReference"

describe("parseReference()", () => {
  test("implicit indexes", () => {
    const segments = parseReference("../../foo")
    expect(segments).toMatchInlineSnapshot(`
      Array [
        Object {
          "field": "..",
          "index": 0,
        },
        Object {
          "field": "..",
          "index": 0,
        },
        Object {
          "field": "foo",
          "index": 0,
        },
      ]
    `)
    expect(segments[2].index).toBe(0)
  })

  test("explicit index to first array element", () => {
    const segments = parseReference("../../foo[1]")
    expect(segments).toMatchInlineSnapshot(`
      Array [
        Object {
          "field": "..",
          "index": 0,
        },
        Object {
          "field": "..",
          "index": 0,
        },
        Object {
          "field": "foo",
          "index": 0,
        },
      ]
    `)
    expect(segments[2].index).toBe(0)
  })

  test("explicit index to second array element", () => {
    const segments = parseReference("../../foo[2]")
    expect(segments).toMatchInlineSnapshot(`
      Array [
        Object {
          "field": "..",
          "index": 0,
        },
        Object {
          "field": "..",
          "index": 0,
        },
        Object {
          "field": "foo",
          "index": 1,
        },
      ]
    `)
    expect(segments[2].index).toBe(1)
  })
})
