import { resolve, ResolvedNode } from "./resolve"
import { transform } from "./transform"
import { parseXml } from "./xml"

export type { ResolvedNode }

export const loadPortfolioPerformanceXml = (xml: string | Buffer): ResolvedNode => {
  const xn = parseXml(xml)
  const tn = transform(xn)
  const rn = resolve(tn)
  return rn
}
