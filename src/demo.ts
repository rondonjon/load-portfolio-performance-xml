import { readFileSync } from "fs"
import { loadPortfolioPerformanceXml } from "."

const xml = readFileSync("data/portfolio-performance.xml")
const data = loadPortfolioPerformanceXml(xml)

console.log("File Version: " + data.client?.[0]?.version?.[0]?.["#text"] || "n/a")
console.log("Accounts: " + data.client?.[0]?.accounts?.[0]?.account?.length || 0)
console.log("Securities: " + data.client?.[0]?.securities?.[0]?.security?.length || 0)
console.log("Example Security Name: " + data.client?.[0]?.securities?.[0]?.security?.[0].name?.[0]["#text"] || "n/a")
