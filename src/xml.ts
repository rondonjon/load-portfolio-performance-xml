import { XMLParser } from "fast-xml-parser"

export type XmlNode = {
  [k: string]: (string | XmlNode)[]
}

export const parseXml = (xml: string | Buffer) => {
  const parser = new XMLParser({
    attributeNamePrefix: "",
    ignoreAttributes: false,
    ignorePiTags: true,
    isArray: (_name, _jpath, _isLeafNode, _isAttribute) => true,
    parseAttributeValue: false,
    parseTagValue: false,
  })

  const result = parser.parse(xml)

  if (!result || typeof result !== "object" || !Object.keys(result).length) {
    throw new Error("XML Parser failure")
  }

  return result as XmlNode
}
