export type PathSegment = {
  field: string
  index: number
}

export const parseReference = (reference: string): PathSegment[] => {
  const segments = reference.split("/")

  return segments.map((segment) => {
    const m = segment.match(/^([^[]*)(\[([0-9]+)\]$)?/)

    if (!m) {
      throw new Error(`Invalid segment "${segment}" in reference "${reference}"`)
    }

    const [, field, , index = "1"] = m

    return {
      field,
      index: Number(index) - 1, // transform to Javascript zero-based indexes
    }
  })
}
