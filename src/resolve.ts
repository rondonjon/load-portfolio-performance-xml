import { parseReference } from "./parseReference"
import { TransformedNode } from "./transform"

export type ResolvedNode = {
  [k: string]: ResolvedNode[]
} & {
  "#text"?: string
}

/**
 * Resolves XStream references in the given `TransformedNode` and returns
 * a `ResolvedNode` as the entry point to a directed graph.
 */
export const resolve = (input: TransformedNode, previousParents: Readonly<ResolvedNode[]> = []): ResolvedNode => {
  const output = { ...input }

  for (const key in output) {
    if (key === "#text") {
      continue
    } else if (key === "#reference") {
      throw new Error("Unexpected #reference")
    }

    const entries = output[key]
    const parents = [output, ...previousParents]

    for (let i = 0; i < entries.length; i++) {
      const node = entries[i]
      const reference = node["#reference"]

      if (reference) {
        const segments = parseReference(reference)
        let currentNode: TransformedNode = output
        let seenParents = 0

        for (const { field, index } of segments) {
          if (field === "..") {
            if (seenParents >= parents.length) {
              throw new Error(`Error resolving reference "${reference}", cannot ascend any further`)
            }
            currentNode = parents[seenParents]
            seenParents++
          } else {
            if (currentNode === input) {
              throw new Error(`Loop detected in reference "${reference}"`)
            } else if (!(field in currentNode)) {
              throw new Error(`Unable to resolve field "${field}" in reference "${reference}"`)
            } else if (index >= currentNode[field].length) {
              throw new Error(`Index out of bounds for "${field}[${index + 1}]" in reference "${reference}"`)
            }
            currentNode = currentNode[field][index]
          }
        }

        entries[i] = currentNode
      } else {
        entries[i] = resolve(entries[i], parents)
      }
    }
  }

  return output
}
