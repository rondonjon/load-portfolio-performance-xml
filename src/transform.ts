import { XmlNode } from "./xml"

export type TransformedNode = {
  [k: string]: TransformedNode[]
} & {
  "#text"?: string
  "#reference"?: string
}

/**
 * Returns a `TransformedNode` of the given `XmlNode` in which all values
 * are `objects` arrays.
 *
 * This allows for typesafe field access as in `root[0].children[0].foo[0].bar[0]`.
 *
 * Eventual text values are transformed to objects with a `.#text` property which
 * can only take a `string` as per type definition.
 *
 * References are also transformed to objects, but with a `.#reference` property
 * of type `string`.
 */
export const transform = (xmlNode: XmlNode): TransformedNode => {
  const output: TransformedNode = {}

  for (const key in xmlNode) {
    const value = xmlNode[key]

    if (key === "reference" && Array.isArray(value) && typeof value[0] === "string") {
      output["#reference"] = value[0]
    } else if (key === "#text" && typeof value === "string") {
      output["#text"] = value
    } else if (!Array.isArray(value)) {
      throw new Error(`Expected an array, received: ${typeof value}`)
    } else {
      output[key] = value.map((entry) => {
        if (typeof entry === "string") {
          return {
            "#text": entry,
          } as TransformedNode
        } else {
          return transform(entry)
        }
      })
    }
  }

  return output
}
